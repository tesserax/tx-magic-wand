var TXMagicWand = {

	init: function(callbackParam,xMaxParam,yMaxParam,xFactorParam,yFactorParam) {

		// X properties declaration
		TXMagicWand.xPosition = 300;
		TXMagicWand.xDelta;
		TXMagicWand.xFactor = -40;
		TXMagicWand.xMax = 600;
		TXMagicWand.xMin = 0;
		TXMagicWand.xAccelerateThreshold = 10;
		TXMagicWand.xAccelerateEnhanceRate = 1.2;
		TXMagicWand.xPosition = 0;

		// Y properties declaration
		TXMagicWand.yPosition = 300;
		TXMagicWand.yDelta;
		TXMagicWand.yFactor = -25;
		TXMagicWand.yMax = 600;
		TXMagicWand.yMin = 0;
		TXMagicWand.yAccelerateThreshold = 10;
		TXMagicWand.yAccelerateEnhanceRate = 1.2;

		// Callback declaration
		TXMagicWand.callback = callbackParam;

		// Custom properties overriding
		if (!(xMaxParam === undefined)) {
			TXMagicWand.xMax = xMaxParam;
			TXMagicWand.xPosition = xMaxParam / 2;
		}
		if (!(yMaxParam === undefined)) {
			TXMagicWand.yMax = yMaxParam;
			TXMagicWand.yPosition = yMaxParam / 2;
		}
		if (!(xFactorParam === undefined)) {
			TXMagicWand.xFactor = -1 * xFactorParam;
		}
		if (!(yFactorParam === undefined)) {
			TXMagicWand.yFactor = -1 * yFactorParam;
		}

		window.addEventListener("devicemotion",function(event) {

			TXMagicWand.xDelta = event.rotationRate.gamma * event.interval * TXMagicWand.xFactor;
			if (Math.abs(TXMagicWand.xDelta) > TXMagicWand.xAccelerateThreshold)
				TXMagicWand.xDelta *= TXMagicWand.xAccelerateEnhanceRate;
			TXMagicWand.xPosition += TXMagicWand.xDelta;
			if (TXMagicWand.xPosition > TXMagicWand.xMax) TXMagicWand.xPosition = TXMagicWand.xMax;
			if (TXMagicWand.xPosition < TXMagicWand.xMin) TXMagicWand.xPosition = TXMagicWand.xMin;

			TXMagicWand.yDelta = event.rotationRate.alpha * event.interval * TXMagicWand.yFactor;
			if (Math.abs(TXMagicWand.yDelta) > TXMagicWand.yAccelerateThreshold)
				TXMagicWand.yDelta *= TXMagicWand.yAccelerateEnhanceRate;
			TXMagicWand.yPosition += TXMagicWand.yDelta;
			if (TXMagicWand.yPosition > TXMagicWand.yMax) TXMagicWand.yPosition = TXMagicWand.yMax;
			if (TXMagicWand.yPosition < TXMagicWand.yMin) TXMagicWand.yPosition = TXMagicWand.yMin;

			TXMagicWand.callback(TXMagicWand.xPosition,TXMagicWand.yPosition);
			
			},true);

	}

}